#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija


if __name__ == "__main__":
    try:
        file = open(sys.argv[1], 'r', encoding='utf-8')
        calchija = calcoohija.CalculadoraHija()
        lines = file.readlines()
        for line in lines[:-1]:
            parametres = line.split(",")
            if parametres[0] == "suma":
                result = 0
                for parametre in parametres[1:]:
                    parametre = int(parametre)
                    result = calchija.suma(result, parametre)
                print("Suma =", result)
            elif parametres[0] == "resta":
                result = int(parametres[1])
                for parametre in parametres[2:]:
                    parametre = int(parametre)
                    result = calchija.resta(result, parametre)
                print("Resta =", result)
            elif parametres[0] == "multiplica":
                result = int(parametres[1])
                for parametre in parametres[2:]:
                    parametre = int(parametre)
                    result = calchija.multiplica(result, parametre)
                print("Multiplicacion =", result)
            elif parametres[0] == "divide":
                result = int(parametres[1])
                for parametre in parametres[2:]:
                    parametre = int(parametre)
                    result = calchija.divide(result, parametre)
                print("Division =", result)
            else:
                sys.exit("""Function can only be
                            'suma','resta','multiplica' o 'divide'""")
    except ValueError:
        print("Error: Non numerical parametres")
    file.close()
