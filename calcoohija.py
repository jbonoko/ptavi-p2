#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo

calc = calcoo.Calculadora


class CalculadoraHija(calc):

    def multiplica(self, op1, op2):
        return op1 * op2

    def divide(self, op1, op2):
        if op2 == 0:
            sys.exit("Division by zero is not allowed")
        else:
            return op1 / op2


if __name__ == "__main__":
    calculadorahija = CalculadoraHija()
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
        operacion = sys.argv[2]
    except ValueError:
        sys.exit("Usage Error: Non numerical parametres found")

    if operacion == "suma":
        result = calculadorahija.suma(operando1, operando2)
    elif operacion == "resta":
        result = calculadorahija.resta(operando1, operando2)
    elif operacion == "multiplica":
        result = calculadorahija.multiplica(operando1, operando2)
    elif operacion == "divide":
        result = calculadorahija.divide(operando1, operando2)
    else:
        sys.exit("""La operacion solo puede ser
                    o suma, resta, multiplica o divide""")
