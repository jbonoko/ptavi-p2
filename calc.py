#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


def plus(op1, op2):
    """ Function to sum the operands. Ops have to be ints """
    return op1 + op2


def minus(op1, op2):
    """ Function to substract the operands """
    return op1 - op2


if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
        operacion = sys.argv[2]
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if operacion == "suma":
        result = plus(operando1, operando2)
    elif operacion == "resta":
        result = minus(operando1, operando2)
    else:
        sys.exit('Operacion solo puede ser sumar o restar.')

    print(result)
