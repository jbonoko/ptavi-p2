#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import csv
import calcoohija


if __name__ == "__main__":
    with open(sys.argv[1]) as csvfile:
        list = csv.reader(csvfile)
        calchija = calcoohija.CalculadoraHija()
        for line in list:
            if not line:
                sys.exit()
            elif line[0] == "suma":
                result = 0
                for parametre in line[1:]:
                    parametre = int(parametre)
                    result = calchija.suma(result, parametre)
                print("-> Suma =", result)
            elif line[0] == "resta":
                result = int(line[1])
                for parametre in line[2:]:
                    parametre = int(parametre)
                    result = calchija.resta(result, parametre)
                print("-> Resta =", result)
            elif line[0] == "multiplica":
                result = int(line[1])
                for parametre in line[2:]:
                    parametre = int(parametre)
                    result = calchija.multiplica(result, parametre)
                print("-> Multiplicacion =", result)
            elif line[0] == "divide":
                result = int(line[1])
                for parametre in line[2:]:
                    parametre = int(parametre)
                    result = calchija.divide(result, parametre)
                print("-> Division =", result)
            else:
                sys.exit("""Function can only be
                            'suma','resta','multiplica' o 'divide'""")
