#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora():

    def suma(self, op1, op2):
        return op1 + op2

    def resta(self, op1, op2):
        return op1 - op2


if __name__ == "__main__":
    calculadora = Calculadora()
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
        operacion = sys.argv[2]
    except ValueError:
        sys.exit("Error: Non numerical parametres found")

    if operacion == "suma":
        result = calculadora.suma(operando1, operando2)
    elif operacion == "resta":
        result = calculadora.resta(operando1, operando2)
    else:
        sys.exit('La operacion solo puede ser o suma o resta')
